+++
title = "Micro, a better nano"
author = ["Renzix"]
date = 2019-11-24T19:10:00-05:00
tags = ["editor", "terminal", "micro"]
draft = false
+++

When you hear about powerful terminal text editors the only real options most
people think of is vim and emacs. Most people who don't use these text editors
claim that nano is the best because its the easiest. You "don't need" a powerful
text editor. I really dislike this ideology because people end up using a awful
editor which can't be extended at all. I can't suggest emacs because of how long
and unintutive its default keybindings. Not to mention that emacs should be used
as a gui app. I can't suggest vim because its modal and requires a large amount
of memorization to use. In comes micro which is made to be as easy as nano and
powerful as vim.

Micro is made in golang so you can easily get a compiled binary on any
architecture that can run go and that tcell supports. Micro has a simple json
config which allows you to rebind keys or set specific options. While this is
nice its just as limiting as something like nano however it also allows the
installation of plugins through lua and define a init.lua which allows you to do
the same things as your json config. The init.lua is actually just a single file
plugin so its just as powerful as a normal plugin. Unfortunately there aren't
many plugins and the few that are available aren't really half as powerful as
some of the popular vim/emacs ones.

One of the options you can set actually shows a nano like keybinding map at the
bottom making it extremely easy to use. Note this is off by default but you can
setup a system config which turns this on. Not only does it do what nano does
but it has actually sane keybindings in 2019. This means Ctrl-s to save and
Ctrl-q to quit. Also Ctrl-c to copy, Ctrl-v to paste etc.

Micro has a command line with Ctrl-e which supports on the fly option setting,
executing lua functions, replacing text and more. This is one of the "missing"
features that nano desperately needs. As of right now the ecosystem needs to get
bigger so it can support more powerful features ootb but this is a awesome
foundation.
