+++
title = "Hugo and Org mode"
author = ["Renzix"]
date = 2019-11-10T23:40:00-05:00
tags = ["hugo", "emacs", "org"]
draft = false
+++

My first blog post using Emacs and Hugo is finally finished! I thought about it
and I think I'm going to store Hugo in a git repository on github to take
advantage of github pages. Also the alternative would be to use nextcloud and
that seems like a pain. Although keeping it with my org-mode TODO stuff would be
nice (will probably be a later blog post about it).

To start off about my Hugo setup I am using the Blackburn theme (bottom of the
left sidebar). Originally I was going to make this separate from my main HTML
setup(blog.renzix.com) but I decided that I probable wasn't going to make a HTML
website for a while and other reasons.

After I choose the theme I setup my configuration which was pretty much a
copy/paste form the example given by the theme author. The real reason why I am
using Hugo is because there is a nice plugin called ox-hugo which allows me to
write my posts in org mode and export to Hugo markdown. This is actually REALLY
nice as all I have to do is setup a couple properties. You just need to setup
`#+HUGO_BASE_DIR` and add the `:EXPORT_FILE_NAME: filename` to the properties
tag of a bullet OR you can use separate files(I think 1 file is better tho).
After that you can set a few more options like EXPORT\_DATE for the date and it
actually intergrates with org mode.

Hugo has this notion of Drafts which in ox-hugo is represented by TODO. This is
a REALLY smart idea. Not only that but tags in org mode work just like tags in
ox-hugo making it even easier to use. Export date is manual from what i can see
but I probably can make a function with org-time-stamp-inactive or something.

If you want to see how I configured it see my git repo at
git.renzix.com/Renzix/Blog

EDIT: I have also found out that if you enable time on DONE tasks you don't even
need to put a date on it making it even more awesome. All you need is a
filename and to mark it as done.
