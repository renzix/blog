+++
title = "Fedora Silverblue"
author = ["Renzix"]
date = 2019-11-16T16:47:00-05:00
tags = ["linux", "fedora", "silverblue"]
draft = false
+++

I recently spent a week trying out Fedora Silverblue so keep in mind I am not
running it anymore and actually installed guixsd (probably a article about that
switch soon). Fedora silverblue is very experimental  and a interesting concept.
It revolves around the idea that there is a immutable base system which can't be
changed easily. This is the biggest aspect that is close to nixos and guixsd
however the way you deal with this immuatable base is very different. Note that
_var_ is the only mutable directory and everything else is symlinked to it
including /home/user.

So when you try to install a package natively you need to use ostree which saves
a snapshot of your current and previous proflies. You can then like nixos go
into previous versions on boot. However when installing new software you have to
actually restart your computer so this method is supposed to be a more permanent
way to install stuff.

The other way to install programs is by using a flatpak. This is nice for apps
that have flatpak support and doesnt require you to restart or anything weird.

Both of the previous mentioned ways to install packages doesn't let you develop
applications. It only allows you to run final applications or would require you
to restart after every new library installed. So in order to solve that
silverblue comes with another application installed called toolbox.

Now toolbox isnt unique to fedora silverblue as its basically a shell wrapper
aroud two container technologies (I forget the names) which allows you to run
containers like they are native. You can create different profiles with
completely different containers but generally you only need 1 or 2. To enter
said container just run

```sh
toolbox enter
```

and the run a application from it do

```sh
toolbox run programname
```

You can add .desktop so that you can easily run these apps instead of having to
run shell commands. Also the default image is a Fedora 31 container so you have
access to anything that fedora 31's dnf has.

I personally ran it on a 3gb 2 core thinkpad and the performance didn't make it
unusable just took a little longer to start applications. Also I have found that
alot of apps I needed like gqrx was not available as a flatpak which was really
annoying. I wanted to keep development ONLY inside containers and applications
ONLY as flatpak but I couldn't. Also coming from arch/gentoo I found it annoying
having to jump through the fedora hoops to run software inside a container not
even natively. Other then these things I found it good. In my opinion this
distrobution deserves to exist.
