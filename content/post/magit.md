+++
title = "Magit is better then git"
author = ["Renzix"]
date = 2021-06-16T13:52:00-04:00
draft = false
+++

I love git. I think one of the main reasons I love git is because magit exists
and makes things much easier. So why would I use a git gui when the git
commandline is so good? Well git is awful when it comes to long or complex
commands. While porcelain makes it easier, there are still some things that are
hard to do within git. For example, if I wanted to clean up a branch in order to
submit a PR(very common thing for me). I would have to

```sh
## OPTIONAL
git status # check if any changes
git stash # if anything is there

## Main
git log -p HEAD~5 # or maybe HEAD...master or a actual commit
# scroll through the million changes and memorize what needs to happen or open another terminal
git rebase -i HEAD~5 # or HEAD...master or a actual commits
## do the rebase in vim
## OPTIONAL
# edit the files with a diff program then
git status # to see what files have a diff
git rebase --continue
git log # (and check if its good)
## at this point I would probably push and check inside the remote
git push
## probably redo it
```

Notice I cant really change anything in this by adding aliases unless I do
something like aliasing git to g (which I honestly probably would do). Lets see
what keybinds I would do in magit.

```nil
C-x g OR SPC g g # open magit
l l # git log
## OPTIONAL
# scroll up/down each commit with C-n and C-p or C-s to search (like through the rest of emacs)
# ENTER to see diffs and another ENTER to goto the file at that exact commit
# q to quit then
r i # to rebase interactively on the current commit
C-n/C-p to move around and actual modal editing like w to reword instead of typing out reword, alt up/down to move commit up/down etc...
## OPTIONAL
e n a # move to the git status then e to open ediff on the file, n to move to next diff and a or b to select either or edit the 3rd buffer (C-x o c ... edit ...)
r c # rebase continue
P u # push
```

as you can see normal git has about 8 commands which all require ~6 characters
on average with context and flags ontop of having to know the entire command
with flags. This also does not include fixing diffs or moving around the diff,
nor does it include opening a seperate terminal or memorizning what needs to be
done. The magit version has like ~20-40 characters depending on how many diffs,
how throughly you want to check it. \`git rebase -i HEAD~5\` has about 20
characters. This is why I consider git command line very verbose when I compare
it to magit.
