+++
title = "Git Hooks"
author = ["Renzix"]
date = 2019-11-11T20:31:00-05:00
tags = ["git", "vcs", "info"]
draft = false
+++

Git has a nutty amount of features so one feature I was completely unaware of
but really found useful is Git Hooks. They are simply executable which are
executed at certain points in the git workflow. Git hooks can either be client
side or server side and both are interesting in what they can do. The location
of said git hooks are in .git/hooks and come with some examples.

As of right now I setup a git hook for my server to automatically deploy this
blog. Anytime it gets a `git push` request it builds my website with hugo and
copies the html to /var/www/html. All of this is automated so its REALLY nice
having live changes show up every time I make a commit and push it to my repo.
Also when you run git push it actually shows the client what the command output
of the server is making it even more powerful.

Git hooks seem like a killer feature and I really need to update my emacs config
to build like this.
