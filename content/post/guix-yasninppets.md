+++
title = "Abusing Yasnippets"
author = ["Renzix"]
date = 2019-11-18T21:26:00-05:00
tags = ["emacs", "yasnippets", "guix"]
draft = false
+++

Recently I have switched to guixsd and found out that it does not have ripgrep,
exa or bat. To me the only one I really care about is ripgrep so naturally I
wanted to learn how to package it. Well... there is alot of bolderplate code
involved in that because I need to package every single crate ripgrep uses.
Looking into the source code for guix they actually do this.

In comes yasnippets a emacs snippets package. Not that its actually based on
text mate snippets (not the same) but integrated into emacs. Basically you can
create a plain text snippet and the only characters which really acts
differently is $. $0 for example is the cursor location if you want to move the
mouse cursor. everything after $0(ie $1 and $2) is run moved to before it.
Basically once you type stuff into it press TAB to go to the next number. You
can also do arbitrary lisp commands either as you type, on TAB or at the start
of the snippet creation.

In guix you need to generate a hash of the crate so naturally I put that
generation into the snippet. I used (yas-field-value 1) and (yas-field-value 2)
to get the name and value (which was previous values typed in the snippet). I
then ran a shell command guix download
<https://crates.io/api/v1/crates/name/version/downoad> . It actually does the
elisp on every keypress however there is a value which is only true on moving
away (TAB). That value is yas-moving-away-p and I just run that command when it
is true. In theory I could also get the license and home-page however I have no
idea how to get json from elisp. The most annoying thing was typing out the name
of the package, version and downloading the hash anyway. See the script [here on
my gitea insance](https://git.renzix.com/Renzix/Dotfiles/src/branch/master/.doom.d/snippets/scheme-mode/rust-package). PS: I probably should make it not a single line xd
