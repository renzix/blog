+++
title = "Greatest Editor Improved"
author = ["Renzix"]
date = 2021-06-25T16:51:00-04:00
draft = false
+++

Now that we talked about vi we can talk about vim and nvim. I am going to group
nvim and vim together because most of the fundementals dont change and the parts
that do change can be explained pretty easily. Note all things in vi apply to
vim/nvim.


## Text objects {#text-objects}

This is one of the most interesting points of vim which is rarely in other text
editors for whatever reason. Text objects are well text areas based on what
keybind associated with it. So you can do diw to delete the inner part of a word
text object. People using vim tend to not realize that the i changes the next
character (w in this case) to do something completely different but the same
based on pnumonics. I didn't even notice that this was different because dw
should actually go to the beginning of the next word but it doesn't and does
what de is supposed to do.

The amount of text objects have is actually nuts and not even vim does a good
job with them. Text objects not only could effect simple things like words and
stuff inside parensthesis but actual structures inside code. Vim lacks the
support to do this without plugins and it would depend on the language so they
don't do anything like this. Vscode and other editors aren't modal so it becomes
much harder to add text objects as they require people to memorize multiple
keybinds and its harder to get someone to press a modifier keybind twice (just
as bad to do 1 modifier keybind and one not).


## Scripting language {#scripting-language}

This is one of the parts where vim and nvim differ. Vim introduced vimscript as
a custom language which could extend the vim text editor. While vim is great for
setting up your configuration and vim-like stuff, it becomes much harder to use
for complex tasks and tends to be pretty slow. After the main dev of vim refused
to add certain features and address certain things nvim was created. When nvim
was created they decided to rewrite vim (still supporting vimscript) but some
things including better defaults and 1st party support for lua instead of trying
to fix up vimscript. Note they still will support vimscipt just they will try to
phase it out. Addressing the issue of vimscript being slow the creator of vim
began creating vim9scipt which is not backwards compatible but significantly
faster then vimscript. This is where we are in the current moment of time.

Now that we got that out of the way vim is mostly written in c but has a
significant amount of vimscript so you can configure it and extend it. Vimscript
makes it easy to run commands when certain files are opened, set keybindings,
set settings and much more. Having nvim support both lua and vimscript allows
for complex plugins to be made in lua. I believe there are multiple plugins that
use this lua and could not be created without the lua support.

Lastly the scripting language makes it easy to add plugins and because
vimscipt/lua is so good, there are many different plugins that do interesting
and useful things. From everything to build systems to lsp to adding new text
objects there is a plugin for almost anything.


## Macros {#macros}

Macros in vim is again amplified by modal keybinds and the workflow of vi.
Because everything you do in vim tends to be percise, you can setup macros which
move arount the structure of what you are editing easily. I do not do them
because it is very complex but you can also do recursive macros and because
macros are just plain text stored in vim registers, you can just make one then
paste it in your config for later.


## Misc nonvi features {#misc-nonvi-features}

vi is very basic so vim has much more modern things such as multi level undo,
syntax highlighting, visual mode, more cross platform and more. The multilevel
undo is pretty good because of the sheer amount of things you can do with it.
Also basic support for programming with ctags.
