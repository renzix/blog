+++
title = "My vim.init"
author = ["Renzix"]
date = 2019-11-25T23:41:00-05:00
tags = ["vim", "keybinds"]
draft = false
+++

Even though I try to shill emacs as much as I possibly can I also like to use
nvim so I can see the competition and learn things. Some things neovim does is
fundamentally better then emacs ie evil. Workflows are fundementally different
and I think that in order to use your workflow to its fullest potential you
should understand other workflows and possible use different ones at different
times. Anyway I recently remade my vim configuration as I originally made it in
markdown trying to emulate org mode. This is not the vim way to do things so I
need to abuse the vim way to do things. The vim alternative is by using folds
for sections and just commenting out the comments. Also I try to be as verbose
as possible ie use color instead of co.

The first thing I do is take care of the plugins. I auto install VimPlug via
curl and github. In the future I need to check if the operating system is
Windows and plan accordingly. The next step is configuring the plugins. I am not
going to go into detail about each plugin that I have as that will be a separate
Blog post or even Video if enough people care. Some plugins that I have are
vim-localvimrc(for projects), vim-which-key, ultisnip, vim commentary, fzf, ale,
vim-fugitive, vim-gitgutter, vim-rooter, and the vivid chalk theme.

Next up I have a bunch of variables which are very nice to have. Relative line
numbers to me are really cool and useful. scrolloff (or so) is another useful
feature which basically makes your buffer scroll down one line when you are X
lines away from the bottom giving you some breathing room. Undofile lets you
save your undo history for multiple vim sessions on any file. For searching I
use smartcase (which should be default not sure if it is), incremental search
and incommand=nosplit which makes it so it shows live changes. I take a stance
and disable tabs as much as I can however I am not a die hard spaces person. I
personally think its fine as long as you are consistant. If I go into a project
which requires tabs I would just use my .lvimrc file to set tabs on by default.
I also highlight tabs, trailing whitespace, and lines that go past 100 in red.
Note that letters are 80 are red however the lines between 81-100 aren't.

For my ex commands I have Cfg which opens up my configuration, a sudo edit
command with :W, and 2 commands to change the default directory. One really nice
keybind which makes perfect sense is binding Q in normal mode to @q. This means
I can qq<some macro>qQ instead of having to add a @q. Q isn't really used for
anything important and this allows me to have a default "temporary" macro key. s
and S are actually useless so I also rebound them to fzf files either in the
root of my git project or my current file directory. | is to switch buffers and
\\ is to search with ripgrep. These are the same as my emacs bindings in evil so
thats why I have them. I am unsure what to bind ; to so I just bound it to
Commands for fzf as ; in evil is what I'm used to anyway.

Next up is my leader keys and they are also interesting. My project keybinds are
all based on a plugin which allows me to create a .lvimrc in the root directory
of my project. This .lvimrc can have different variables or settings which I
define. I have a variable (which is defined as make) called g:compile\_command
which gets run through my AsyncRun plugin so I can compile my project. I can
also type <leader>pe in order to edit said .lvimrc. My git-fugitive one is
pretty simple and is just a bunch of bindings I find useful. I purposely run
:Gpush without the <CR> if not a capital P so that I can choose which remote I
want to send it to. I may have a g:git\_remote variable in the future if this
gets too repetitive. Some other stuff is lsp which is pretty standard and a
keybind which copies whatever is in the " register into the + or \* register
respectively. If you know vim then you know these registers are for the system
clipboard and primary clipboard. So I can do yy<leader>+ to copy the current
line to the clipboard instead of "+yy which isn't that much worse but requires me
to think of that before the copy.

Next is which-key which is really nice and really just bolderplate. For my
future vim config I think I need to stop using my emacs evil bindings and start
thinking more vimy. Neovim is NOT emacs and won't ever be(it might keep getting
more emacsy tho :p). Also I haven't really played around with ultisnip too much.

PS: you can find my vim config [here](https://git.renzix.com/Renzix/Dotfiles/src/branch/master/.config/nvim/init.vim) at my Dotfiles repo.
