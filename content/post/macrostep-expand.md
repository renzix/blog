+++
title = "macrostep-expand"
author = ["Renzix"]
date = 2019-11-12T18:53:00-05:00
tags = ["emacs", "macro", "elisp"]
draft = false
+++

I recently found a really nice function which allows you to expand macros
temporally. Basically it allows you to step into the macro to see what it
expands too. Doom uses alot of macros so this is really interesting to me. map!
actually expands to general which tbh is expected. use-package! and package!
package! is alot more interesting and does a whole bunch of listy things. I need
to write something that abuses lisp macros because its really nice to work with.
