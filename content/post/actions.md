+++
title = "Actions"
author = ["Renzix"]
date = 2024-01-16T20:02:00-05:00
draft = false
+++

## Overview {#overview}

There are multiple different ways to give a editor a action. In my opinion the
most helpful way to look at what an action needs to be bound to is by looking at
the following ways. Some of these can be argued to be the same but in my opinion
that misses the point. I will probably go in depth on why these feel the same
later in the post. Note that a bind can have one or more of these and most
editors support multiple of these.

-   modifier: a modifier key which is held down while pressing another key. Think control + s for saving or shift a for A.
-   prefix: press a key then go to another key. Think the leader key in vim OR control k in vs code.
-   modal: the entire keyboard map changes depending on the current mode. There are dedicated keys to switching the mode. Think caps lock or vim normal/insert mode.
-   context dependant: the function that gets called reads the current state of the program and does different actions depending on that state. This means if I delete a symbol in a c file that would be different then if I deleted it in a bash file. A simpler example is how vscode deals with C-x. If text is highlighted then it's cut but if no text is highlighted then it's delete the current line.
-   key chords: pressing certain keys at the same time or in quick succession. This is not used much however one example is typing jj(quickly) to exit insert mode in vim.
-   special keys: having a dedicated key to a specific action that is used frequently ie print screen.

(Everything above is what I would consider a way to implement a keybind.
Everything below is other ways to implement a action)

-   command: having a way to execute a command. There imo different types of commands but I will get to that later. Can be something like search, fuzzy finder or something like vims command mode.
-   mouse/visual: allows the ability for you to have visual representations of what things do and for you to click on them.

Now each one of these has their pros and cons. I will go over detail each one in
future blog posts, how they can interact with each other and finally how they
are used in emacs, vim, vs code and any other random editor I may mention.
Obligatory THIS ISNT GOING TO MAKE YOU A BETTER PROGRAMMER, it will make you
able to use your editor more efficiently. If this isn't a problem for you don't
feel the need to tell me or argue how XYZ is too complicated or a waste of time.


## Modifier {#modifier}

This is probably what people hear when they think keybind. Because most
computers have a control, alt and windows key built in its used frequently and
people already know how to use shift. Modifier keys work well when used
spairingly and work pretty decently with prefix keys of the same type (ie C-x
C-s in emacs or C-w C-w in vim). Let's go over the pros and cons of this style
of keybind.

Pros:

-   Every keyboard already has a Ctrl/alt key dedicated for this
-   Simple 2 keys = 1 action and no keyboard state
-   if you are holding control you don't have to repress it when you want to do 2 or more commands ie emacs C-x C-s is 3 keys.
-   the best way to do 2 actions without dedicated keys followed by typing or other common keys like normal mode commands
-   very easy to add in at a whim

Cons:

-   Basically every keyboard puts Ctrl and alt in a very awkward spot so you may need to remap it for good ergonomics
-   certain OS's use certain modifiers (I'm looking at you macos with your command key) so they may different based on OS
-   multiple modifiers is very ugly and should be avoided at all costs
-   if you have 1 modifier you should not switch to another ie C-x M-s is awkward and bad. This also applies to removing a modifier as well (C-x x C-x is also ugly). This means you probably only have like 50 or so commands (with no modes only alphabet and 2 modifiers).


## Prefix {#prefix}

Prefix is very interesting. One of the nicest things about prefixes is that once
you dedicate a single key you do not have to touch any other keys and get the
entire keyboard to do different actions. The drawback of this is that it's only
until you do a action then you have to redo everything. By itself you need a way
to get into that prefix state so it really needs one of the other methods or for
you to do something like make j your prefix and have jj insert a actual j. On
top of this you can choose to have 1 prefix with a bunch of prefixes inside of
it or multiple prefixes for different things. Vim for example has a prefix key
for windows C-w and tells the user to have a generic catch all prefix with the
leader key.

Pros:

-   Best way to run a single action without a dedicated key.
-   easy to organize
-   supports a catch all type approach (leader key which does everything) with a single key.
-   supports virtually unlimited actions
-   makes mneumonics easy if organized properly

Cons:

-   if you use a single prefix key then you will always need to press more keys then the alternative for an action.
-   bad if you want to do multiple actions of the same type (ie I want to save a file then open a new one)
-   requires a way to open the prefix. In some editors this is easy, others it's hard or annoying. Specifically in modal editors it's easy to dedicate a specific key. If you are in a constrained keyboard then you need to use ctrl or alt like emacs does with C-x.
-   bad if unorganized. I hate emacs for the fact that C-x 8 e l exists. Don't ask me what it does.


## Modal {#modal}

Modal editing is probably the highest skill cap. The real benefit of using a
modal system is that you can do multiple actions of the same general type
without doing anything fancy. This generally mimics how people interact with a
computer as well. Generally when we type, we tend to type full sentences and
when we edit, we make small quick changes then move around more frequently. Imo
the biggest downside is that you have to be aware of state while doing this but
this can be avoided by having one "normal" mode (pun intended).

Pros:

-   best way of doing multiple like commands, one after another.
-   generally the highest skill cap way to do editing given you can do all like actions at once
-   works very well with other methods.

Cons:

-   requires learning multiple different key maps which tend to be harder then doing a basic prefix
-   requires a separate "useless" action per mode so you can change modes
-   commonly has multiple "useless" actions for the same mode change to make the change seemless (ie dw vs cw in vim both delete a word but cw also changes the mode)
-   requires keeping track of the current mode or to default to a specific mode (so transitioning from insert mode into replace mode is generally painful)


## Context Dependant {#context-dependant}

This trick will allow the user to reuse keybinds for the same thing. While this
doesn't seem like much if you do this multiple times for every key, you can end
up with half the keyboard free or more efficient binds. The biggest downside is
that you can't do anything everywhere but only anything anywhere, with the need
to either suck it up and do it manually or have a extra keybind somewhere else.

Pro:

-   Requires NO new keys
-   generally easy to understand
-   requires knowledge of action being different depending on state

Cons:

-   the action needs to be mutually exclusive with another actions
-   harder to implement generally
-   if you do it too much you might get unexpected results
-   may need a fallback keybind or way to access a action anyway


## Key Chords {#key-chords}

Pressing one or more keys in rapid succession is a interesting concept but hard
to get right. No editors that I know of use this efficiently but some like vim
and emacs(with plugin) supports it. The most useful aspect with this is that it
doesn't require any keys or modifiers so it can be used if you are unable to
bind any more modifier keys or letters but still want something faster then
command mode.

Pros:

-   requires no keys and doesn't have to be in a prefix, modifier or modal keybind.

Cons:

-   you can arguably use a prefix for a similar result (ie jj can just be j j in a prefix)
-   requires a delay when you type the aforementioned key
-   requires you to not want to type a sequence (like jj) or for you to wait until the delay times out.
-   more useful outside of a prefix or modal mode where your keybinds are very limited


## Special Keys {#special-keys}

Special keys are keys which are designed for a specific task. The f keys are a
good example of this as they are made to be rebound in most editors. Also stuff
like the home and end button. They work fine but it doesnt scale like all the
other solutions.

Pros:

-   single key single action
-   simple to understand and easy

Cons:

-   keys are far away from home row. More you add the farther they are
-   extremely limited number of keys
-   not every keyboard has these keys


## Command {#command}

This is the more lengthy way to access a command or do something. I'm going to
separate this into three different ways. You can probably argue these three are
all different. The first way is simply search. This way uses the information
given to go to a specific spot, you can also put any other degraded modes like
replace in here. The second way is menus. This will open up a menu and based on
what you type in it will select a action and execute it. Normally this is a
fuzzy menu. The last way is code and it's when the input is executed as some
programming language. The difference between menu and code imo is that code can
have real logic along with arguments being entered in.

Pros:

-   with code you can do literally anything
-   unlimited amount of commands with no thought as to where to put them (unlike prefix which requires organization and muscle memory)
-   allows for a nice visual of what you are going to do

Cons:

-   VERY slow compared to any of the above methods
-   requires some sort of prefix to enter this mode (ie : in vim)
