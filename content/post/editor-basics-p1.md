+++
title = "How text editors work"
author = ["Renzix"]
date = 2021-03-22T17:02:00-04:00
tags = ["editor"]
draft = true
+++

I am going to attempt to talk about how text editors function in a general sense
and over a couple posts. After we define ui, input and functionality we can
start going over specific editors and what they have/tradeoffs they give. I am
going to take from my knowledge of a whole bunch of power user text editors
(Vim, Kakoune, Emacs), Easy to use text editors (sublime, vscode, atom) and
IDE's (eclipse, visual studio, intelleji).

UI is basically how the editor looks. Input is how the user interacts with the
editor. This can include shortcuts, the mouse and ways different states effect
both. Actions are the things that happen after a input.

After that will be a comparison of each editor. This comparison will have all
three things defined above with the attached potential and final. Because
plugins exist, the configuration will be in the potential bullet. The final
bullet will have a general overview on what I think.
