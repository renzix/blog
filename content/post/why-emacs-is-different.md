+++
title = "What makes emacs different"
author = ["Renzix"]
date = 2022-03-19T22:10:00-04:00
tags = ["emacs"]
draft = false
+++

I am suprised I never talked about this on this blog so here is some content
about why you should care about emacs and what you can take away in comparison
to other editors. There are more things but here are some of the big ones
emphasised in emacs.


## Customizability {#customizability}

Emacs is extremely customizable because it fundementally is a lisp interpreter
at its core. Unlike other editors whom have a plugin system built ontop of the
editor, emacs is mostly elisp which is used to configure the editor. This means
you are a first class citizen and get access to ALL the functions that the
creators do. Because it is a lisp it is also very easy to get access to those
functions and change them. You can add a function to run after any other
function you might want to edit for example.


## Easy Iteration {#easy-iteration}

In emacs its extremely easy to add configuration on the fly. Again because it is
a lisp interpreter at its core you can just change how things work by just
writing some code and executing it anywhere. For those that dont know emacs even
has a builtin keybind for this \`C-c C-e\`. Most other editors you have to either
visit a specific config file or restart the editor. With emacs you can literally
just type anything and run it. Also the configuration is very easy to learn/use.
Emacs gives you plenty of tools to find descriptions about any function or
variable that exists.


## Org mode {#org-mode}

Org mode deserves its own bullet because of the sheer amount of things you can
do. All org mode is, is a mode in emacs originally developed for organization.
One thing which feels bad while using markdown is that its just a fancy way to
display text. Org mode fixes that by adding a insane amount of features which
lets you interact with the text on the page in multiple ways. Want to
expand/contract bulleted points? Sure. Want to have embedded code which will run
in multiple different languages with multiple different options of how they run?
Sure. Want to move bullets of text with a single keybind? Sure. Want to have a
TODO list which pulls from multiple different files with dates (or no dates)?
Sure. Want a entire spreadsheet program in whatever language you want (or calc)?
Sure. Want links to run code or applications inside of emacs or even just a
website link? Sure. Want to export everything above to LaTeX, pandoc, markdown,
html, plain text, utf8 text or some other formats? Sure. I can give out some
more things but I think I made my point.


## Magit {#magit}

Magit is simply a easy to use and intutive interface for git. "Why do I need a
interface for git? Isnt the terminal good enough?". Yes git is a wonderful tool
but one of the problems with it is that it takes WAYY too long to type
everything out. See my other article on magit where i go into more detail but
TLDR git is very verbose. Magit fixes this by giving the user a wonderful visual
representation of the git status as soon as you start it. From there you can
goto each diff instantly by keybind, press tab to show/hide and use context
aware keybinds to do stuff like stage or unstage the diffs. To do this in git
you would need multiple different commands and you would have to scroll up/down
your terminal to see all the diffs. Not to mention you can even stage partial
hunks of the file (actually builtin feature of git that nobody uses because its
too verbose). Ontop of this there are nice shortcuts which are based on what the
command is. c c would commit everything (the same as git commit). How ever ontop
of that it is context aware and has some nice NEW commads you can use. I
frequently use c e which removes the current commit then readds that commit with
staged changes. You can even add flags to that by doing -X where X is the flag
name. Lastly because I want to keep it as short as possible you can just press
enter on a hunk inside of the git status and it will bring you to the spot in
that file(also diffs being builtin are nice too).


## Incremental Completion Frameworks {#incremental-completion-frameworks}

This is kinda like your sublime control p's for emacs. The difference between
emacs and sublimes is that emacs has multiple for different tasks and not just
one or two of them. Want to open a file? You can use fuzzy finding for ANY file
on your system not just inside a project. Want to run a function? You can use
fuzzy finder to see what functions you would like to run. Want to select a grep
result? You can fuzzy find and get the selection for all of them. You can even
pretty easily make your own WITH entirely different keymaps. This means you can
delete a file inside the completion framework OR open it depending on what
keybind you pressed. Helm even takes this further and lets you pick a file then
do a new fuzzy find for what action you want to do on said file. You also have
multiple options 2 of the most popular being helm and ivy but others exist.


## Keybinds {#keybinds}

Emacs allows you to bind ANY key you want to ANY function you want. While this
doesnt seem very different at first you have to understand that emacs is just a
lisp interpreter and all of the internal code is exposed. You can easily make
context aware keybinds as you have access to pretty much everything thats going
on while its running. This paradyme is used heavily in emacs plugins to conserve
on the amount of keybinds you have to remember. Want to open/close the a bullet
with tab in org mode? Sure! Contextual aware keybinds should be used WAY more,
even inside of emacs because they generally are easy and intuitive.
