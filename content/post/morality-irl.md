+++
title = "Living morally in the real world"
author = ["Renzix"]
date = 2021-10-21T19:29:00-04:00
tags = ["philosophy", "morality"]
draft = false
+++

The world is full of objects and people. The world is also full of information
related to objects and people such as emotion and details. This incedible
concept is insanely hard for people to grasp because we as humans are not
built for understanding how to interact with so much information. So how do we
do it?

We guess.

Humans are great at guessing. The problem with modern society is that when you
make a guess on the internet, the guess is there forever. This guess ends up
shaping who interacts with you(for and against) making you more extreme one way
or another.

One of the things you can do is understand that everything you say and think is
fundementally a guess. This has a whole bunch of consequences. One of the things
you can take away is the idea of a margin of error. Because its fundementally a
guess you have to provide some margin of error. One of the defining parts of
someones morality is their margin of error. Having no margin of error is pretty
objectively wrong because that assumes you are not guessing and that the world
is not infinitely big/complex. Now you can have a extremely small margin of
error effectively treating it like no margin of error.

The other area you can look at is practicality. One of the most important things
to understand here is the sunk cost fallacy. The sunk cost fallacy when applied
to morality is basically vengence. A great example of this is someone being
punished for a crime. If you can 100% guarantee that the person being charged
with the crime will not kill anyone else and will live a good life after death
it is NOT practical to punish said person. Ofc this situation will not be
possible because 100% guarantees dont exist (which is why we have to guess in
the first place) and people who do crimes are more likely to do more crimes.

Practical morality is vastly different from theoretical morality so please next
time you lose your temper or say something awful understand that you are just
guessing and maybe you don't know the whole story
