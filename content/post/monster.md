+++
title = "Monster"
author = ["Renzix"]
date = 2024-01-12T20:22:00-05:00
tags = ["story"]
draft = false
+++

(revised january 31st)
As the air flows to the east through the cage door a whistling sound can be heard. Inside this cage is a monster but unlike normal monsters who roam the earth this monster stays here.
One day a hiker named John was hiking in this woods and heard weeping. With each step the cries grew in intensity. Along  the trail he gazed at a steel door with four sturdy bars. This was the source.

"Why are you weeping?" asked John. A rustle echoed through the air as if a monster was getting up. Fearing for his life a bit John started backing away. "I recently lost my friend" communicated the monster in half broken english due to crying so profusely. "Unfortunately I also cannot leave this cage" said the monster. John felt a twinge of embarrassment because he backing away from a weeping creature. "What's your name?" asked John trying to confort the monster. "I don't have a name nor do I need one" said the monster. Confused John explained "I need to call you something? How about Mound for the mound you are stuck in?". After a brief pause thinking about this suggestion the monster said "Sure".

John and the monster then talked for hours about Johns adventures exploring the world. The monsters laughter resonated with the trees causing them to shake in excitement. In the confines of the prison you could even see a faint white smile come from the monster. Unfortunately for the monster this could not last forever and John needed to leave before it got dark. They made a promise that night for John to come back every month and so John did. He would tell amazing stories about the world to the monster and mound would be elated. One day John asked "Mound, why are you stuck here? I have heard your laughs and they echo across the forest! Surely you could break open this flimsy door.". Mound murmured with a monotone voice full of pain "I cannot leave here". John decided to trust Mound and to not push him on this any further.

After several more months of John's faithful monthly visits, there came a night when he arrived later than usual to share his tales with Mound. As he told his stories to Mound, suddenly a lone wolf jumped at John. Mound hearing this busted open the steel door propelling it directly at the wolf causing it the fly into a tree. In shock and awe John looked at his savior. Mound was a humanoid figure with super human characteristics. Once he noticed the wolf was dealt with Mound quickly scuttled far back into the cage and asked John to go home for the night. John was terrified that had happened to him reluctantly agreed.
The next month John spent wondering, why was that man locked up? He seemed so kind and genuine when he listened to his stories. Determined to help him get out, John decided that nduring his next meeting he will venture into the cave to have a warmer conversation.

The next month rolls by and John returned to the prison. "Hey Mound I want to come in to talk to you this time" asked John. Instantly and firmly Mound says "No". "Why" John says perplexed. Now uncomfortable Mound says "I do not know". Not wanting to press him any further in fear of making Mound uncomfortable he conceeds with a simple "Ok".
The next month the same conversation happens. Then again the next month and then again the next month. Finally John iritated with this answer declares "if you do not know them I am coming in". Determined John enters the prison. Mound being quite nervous is apprehensive about this but stays put. "See" says John. "Nothing bad will happen". John starts telling his stories and can really tell Mound loves it through his expressions. Eventually it's time for John to leave and Mound unexpectedly pleads "can we do it inside next time as well". "Sure" says John.

The next month arrived and John stepped into the prison once more. Mound and John start having a conversation about living in the confines of the cave all the time. As the previous conversation concludes John started transitioning it into the stories about the outside world. The monster rips Johns head off. Mound starts to weep
