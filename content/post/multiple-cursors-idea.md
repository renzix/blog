+++
title = "Multiple Cursors done right"
author = ["Renzix"]
date = 2019-11-13T19:18:00-05:00
tags = ["emacs", "multiplecursors", "keybinds"]
draft = false
+++

Recently I actually found out a interesting way on how to deal with selection in
emacs. A while ago when I was trying to force myself to use pure emacs
keybindings I had trouble with finding a solution I liked which worked like dd
in vim/evil. I then proceeded to install a package which made something I
thought should be default in emacs which was C-w and M-w default to a single
line if nothing was selected.

On my never ending quest to be actually good at emacs I found out what C-w and
M-w actually do and was really annoyed that it was basically useless to me
because I never knew where the previous point was. I found this package which
shows the latest mark and turned on a couple settings and now I have a "second
cursor". This isn't like sublime text multiple cursors where its just another
word for showing live changes in a find/replace and most of the time requires no
setup.

In emacs like vim there are marks which point to a location in a buffer. I
unlike vim emacs has actual highlighting support and all it does is put down a
mark and move your cursor. Now if you always have selection on but its not
visible you can get what I have. So that means typing characters extends the
selection, moving also does, and it seems like you only "leave behind a point".
This point can be changed, deleted too, switched to, copied too, moved around
and more.

To move the mark to my cursor I can just C-SPC while to swap my cursor and the
mark I can just C-x C-x. Interestingly emacs plays well with the idea and leaves
behind a mark whenever I do something like isearch where I can jump to certain
areas.

I need to see if this idea of having multiple cursors can be done with read only
cursors and maybe have them change with the context in which I am in. With my
current method I can only have a single mark but with multiple marks it would
make this EXTREMELY interesting. The other thing is once we have multiple marks
in theory we can change them with context ie one of the end of the method so I
can just M-2 (active mark is now at eol) C-w so i can delete to the end of the
method.
