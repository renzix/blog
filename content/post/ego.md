+++
title = "ego"
author = ["Renzix"]
date = 2021-04-01T22:31:00-04:00
draft = false
+++

One of the thing that I feel a lot of people get wrong is this idea that abstact
concepts are always complicated and should be treated as such. One example is
public speaking. When you are speaking about a subject publically, it only
matters how much people understand and get interested from it. A lot of meaning
tends to get lost to people getting passinate in a topic or trying to sound
smart. As someone who is passionate about a lot of different topics, its really
hard to provide real value to people(and in turn yourself). From my experiance
the more complex of a topic you have, the less useful it is.

This type of thinking breeds ego and elitism. A great example I have found is
when someone says, "How do I use X software" in a random discord server then
someone says "You should use this software.". Sure, that could help and they may
have a point, but most of the time this has nothing to do with the original
question.


## The absurdity of being wrong {#the-absurdity-of-being-wrong}

One thing that I have learned is not just to admit your wrong when your wrong
but being able to do something that you whole heartedly disagree with. The worst
part about being stuborn is that you know, you are going to be wrong but you
never believe you are wrong in a particular instance. If someone doesn't
understand this, they will tend to be rude to the other person and belittle
them. I guarentee you that if you go up to someone and ask if they have ever
been wrong, they will say yes BUT if you ask them a question that they believe
to be right in, they wont budge. If you push that person too far, they will
generally either walk away or if they are stubborn, insult you.

Society is amplifying this through social media because they fundementally CANT
walk away as its much more difficult. It doesnt help that anyone can talk to
anyone in the entire world which is FULL of different cultures and different
types of people. The first way to fixing this is accepting it as fact.


## Social Media {#social-media}

Expressing a opinion on social media is normally what gets these things started.
The trick is to not express a opinion and have a normal conversation where you
talk about experiences and what they saw actually happen. Note that I said saw
because news media has gotten to a point where it can't be trusted for REAL
information. Instead we have to rely on statistics from the source or real life
situations. It should never be a "he said, she said" type of moment. Also keep
in mind one person might be in a area different from yours so it might be a
local problem, not a systemic one.

The problem about government is that it moves so slow that you might as well try
to fix the problems in the local government then show stats to the higher ups
explaining the systemic problems and how you fixed them. Arguing on twitter is
SO bad for the actual problem because the poster is generally satisfying their
ego by talking about the problem and not solving it and you get people with egos
taking offense who automatically believe the oppisite of what you say.

People saying "This should stop" or "This is disguisting" are 100% feeding ego
when they say that. Instead of saying that they should post recipts to charities
of people who know more then they do about the issue truly trying to solve it or
even post well researched articles about the issue and saying catchy statistics
like 60/100 people or something. If the person doesnt have money they can donate
their time/effort. If they dont have that either then they are wasting their
life away and really dont care about the issue.

This was a weird rant.
