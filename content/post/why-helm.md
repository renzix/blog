+++
title = "Why I use Helm"
author = ["Renzix"]
date = 2019-11-13T18:01:00-05:00
tags = ["emacs", "helm", "ivy", "ido"]
draft = false
+++

Helm to me is a core part of functionality and I couldn't imagine using emacs
without it. There are 3 popular fuzzy completion frameworks for commands in
emacs. ido, ivy and helm. ido is builtin and the least intrusive. Basically
vanilla ido ![](/image/ido-mode.png)

There are hacks to get around this inline like fuzzy matching but at that point
just use ivy. It also has somewhat weird keybindings with C-s and C-r to go back
and forth between results. It kinda makes sense because emacs uses C-s and C-r
to go back/forth search results.

Ivy actually has less code then ido (doesn't mean its faster) and instead of
matching inline it matches up/down. Its basically meant as a hackable
replacement for ido and it really succeeds at that. It supports a ton of options
on par with Helm. The philosophy is very different though and I will get into
that next.

First I need to mention that helm is only partially maintained ie no new
features are going to be worked on or added. Eventually people will have to
switch to ivy or have to contribute as its mainly the work of a single person.
The key concept behind helm is that you can do multiple Actions in a single
command (Note while you can do this with ivy its not meant for it). With default
keybindings helm actually rebinds TAB to select a action so you have to either
use RET or C-j for tab completion. RET is selection and C-j is what most people
consider TAB completion. Also in something like find-files you can just press
RET instead when you want to go inside a directory.

At first this seems stupid but once you start understanding it you see how
powerful it becomes. You basically get file manager level control over a single
`find-files` command. Its also easy to remember keybindings because you don't
even have to. If you wanna delete a file and you don't remember M-D just select
the file, press tab and search for it. It even tells you what it is bound too in
the search so you can remember for next time. A while ago I even had a
keybinding to switch from find-files to projectile-find-files so that if i am in
a git repo and I want to move files around then go to a specific file I can
easily switch.

People don't realize that keyboard driven file-managers don't need to bind 30+
keys as nobody will ever remember that. Instead we can just make the entire
file manager fuzzy searching and bind control/alt to the 5 functions we actually
use. Although this is more about helm-find-files and files managers THIS IS JUST
ONE COMMAND. You can literally do this to any other aspect ie creating,
deleting, and editing buffers too.

To me one of the most special things about good software like emacs or vim is
that it is a simple concept and has potential for much greater things. Helm is a
package and a idea about fuzzy finding that has that. Something like ivy is only
really a partial idea of what it could become. Ivy however is hackable,
maintained and a better code base so eventually when I have some free time I may
make my own version of helm within ivy.
