+++
title = "Future"
author = ["Renzix"]
date = 2019-11-11T16:10:00-05:00
tags = ["me"]
draft = false
+++

I made a little "powerpoint" using org mode (exported reveal js) on github. The
information is available on [github](https://github.com/Renzix/Lectures). I think I am going to start making more
things like this but instead of posting it on github and presenting it just make
it on my blog. The reason I made it was to present to the comp sci club at my
university so maybe I will keep that repo for presenting and have this blog to
just have things I have done in its raw form. I will probably get better at this
once I have a good working setup.
