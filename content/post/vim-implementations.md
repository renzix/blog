+++
title = "Vim Implementations"
author = ["Renzix"]
date = 2019-11-20T19:59:00-05:00
tags = ["emacs", "vim", "neovim", "evil"]
draft = false
+++

Vim is a amazing text editor. The way the keybindings flow together is really
something beutiful. I am going to assume most people who are reading this know
what vim is so I am going to explain why emacs is a better vim then vim itself.

One common idea is that vim is fundementally better then vi. People don't really
take the time to understand why its really better. The same thing happens with
neovim to vim but lets start with why vim is a better vi.

First of all vi is more of a standard then anything. If you are going to use vi
you have to pick a implementation. Note that the original vi still works however
there are much cleaner versions like busybox vi or nvi. Well what makes vi
fundementally worse is the lack of vimscript. It allows users to actually change
key parts of the editor to suit their needs more. It is actually generally
encouraged to remap keys which you don't use very often. I think that a editor
which is specifically built for YOU will always be better then a editor built
for everyone. Vim is also about the power users. These power users should be
able to change their editor to suit their needs. Following a "ideal" keymap is a
stupid idea because a ideal keymap doesn't exist. There can be concepts which
people can use but keymaps can be messed with.

Note that I do not consider someone who uses vim on occasion to edit a server
file a power user. I personally think for that reason vi is perfectly fine as
the gains you get from vim aren't really significant. I personally don't even
care if you use nano or another text editor in that case. Vi would just be a
niciety.

Neovim is a complete rewrite of vim's core because people got fed up of the
BDFL. Some problems it originally fixed was async, a awful code base (supports a
insane amount of archs), and awful plugin support. Neovim actually supports lua
as a first class citizen. Vim8 finally released async support however that was
inspired by neovim. Neovim also has alot of subtle things which vim does not
have ie supporting 3rd party clipboard programs instead of a builtin mess.

In comes emacs which is a step up in some areas but a step down in some areas.
Emacs is more disk space, more resource intensive, more complicated, written in
elisp instead of C, not async at its core and many other things. Now this is not
why I think evil is better. Emacs is actually what vim is to vi. It has
fundementally better plugin support and ideology behind how its written. Emacs
is written in elisp and can be extended by elisp while vim is written in C and
can be extended by vimscript.

Emacs can fundementally change core features about it which allows people to
make amazing plugins. You also have the entire library that emacs is written in
to write said plugins. While something like neovim improves on the plugin
support the underlying software is still built in C. One great example is the
idea of evil. Emacs evil litterally emulates vim inside of emacs. Yes it is not
a 100% emulation but its enough for the majority of modern vim and it works the
same way. It even does ex commands nearly identifical to the actual vim command.
By default certain evil commands aren't bound to a key like they are in vim but
you can manually bind them yourself.

One problem with vim is that when you use it on another machine you don't have
your configuration and plugins. Emacs actually has a simple and elegant solution
of this called TRAMP. TRAMP allows you to open a file which isn't even on your
computer. You can actually use TRAMP to open files through SSH without lag until
the saving of the file. It simply just downloads a local copy and uploads the
changes. It doesn't just support ssh but also scp, rsync, sambda and more with
gvfs (including google drive). Not only can you edit said files but its actually
integrated with find-files so you can search or tab complete said files too!!!
Like almost everything inside emacs you can actually configure it and even make
new protocols in your config.

Spacemacs and Doom emacs are configurations which show what emacs truly can do
so if your looking into what else emacs has to offer look there. There are many
more amazing plugins. By default both of those use evil but you can use them
with raw emacs bindings if you want to.
