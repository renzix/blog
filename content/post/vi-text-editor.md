+++
title = "The greatest terminal editor"
author = ["Renzix"]
date = 2021-06-23T20:46:00-04:00
draft = false
+++

Vi is one of the 2 oldest relevant text editors. Vi is also probably the most
unique actually good text editor and inspired many other text editors due to
some of its key features. The point of this series is to show what you can take
away from different text editors so lets get into what makes vi special(hint:
its alot). This will be more about POSIX compliant vi and not vim because there
are alot of different features in vim that aren't in POSIX vi.


## Modal {#modal}

Everyone who knows anything about the everchanging vi, knows that vi is a modal
editor. Well to those who don't know what that means, it means that vi has
different modes. Each mode is supposed have keybindings related to what you want
to do. This lets vi have a seperate Insert and Normal mode. Obviously in insert
mode, you can insert text. Whats not obvious is that in Normal mode, you aren't
supposed to insert text\*. Normal mode has tons of movement and editing commands
on a single key press. Vi does modal correctly by having normal mode, the mode
your supposed to stay in the most. I could talk about the different modes for
hours but the one other thing I want to mention is that insert mode is very
similar to how most people type/think. Most people tend to type multiple letters
(ofc they do because words require more then 1 letter) and most of the time
multiple words which works wonderfully well with insert mode.


## Grammar {#grammar}

Vi has this idea of motions and grammar which makes it much more powerful. In my
opinion this is necessary for a good editor. The way vi codes this is another
hidden mode called operator mode. So the idea of grammar in vi starts with a
action. One very common action is d (delete). You follow a action with a motion.
A motion is programmed to be the same as movement in normal mode. e for example
moves to the end of a word, which means de deletes from the cursor to the end of
the word. Now you can also prefix any command with a number to repeat a command
that many times. Combining this you can do stuff like 3dd to delete three
lines(repeating the action acts on the current line).


## Movement {#movement}

Movement in vi feels much better then most other editors because of it being
modal. Being modal allows for a gigantic amount of keybinds which are easily
accessible. These double as targets for actions as said above in Grammar. Vi is
also somewaht contextually aware allowing you to move forward sentances or
words. There isn't any movement for specific languages which is good/bad since
it is completely portable. The other nice thing is very fast/easy access to
search with //? and n/p.


## Ex Commands {#ex-commands}

Vi has a Command mode which lets you run ex commands. This is kinda unique to vi
or vi based editors with how it approaches this command mode. Vi command mode is
unique because it existed in a world without autocompletion. Because of this
they took an approach of making commands abbreviated. Stuff like w stands for
write and you can just use the shorthand. The other weird thing about Command
mode is that it can do both complex edits or commands with parameters alongside
common functions like saving or opening new files. It can do this because it
just parses the syntax as ex (which the name implies). So you get the access of
a line editor in a visual editor and the visual editor keeps some of the core
capabilities (which aren't accessed very often) in the line editor.
