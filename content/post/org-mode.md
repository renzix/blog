+++
title = "Org Mode"
author = ["Renzix"]
date = 2021-05-26T12:28:00-04:00
draft = false
+++

Emacs org mode is probably the best text based file type in existance. The
beauty of org mode is not in how its written, its the functionality it provides
and the problems it can solve. I don't like explaining the features of org mode
as seperate things but rather the functionality and use cases it can provide.
One important thing to note is that none of these features are unique JUST to
org mode. What makes org mode great is the fact that all of these features work
together and work together extremely well. This is why I believe that emacs is
the best text editor. You could implement markdown text mode that interacts
with a spreadsheet program which supports a TODO agenda protocol which all have
consistent keybinds. In this case you would need to implement a text editor or
plugin, spreadsheet programs which interfaces with said editor, code execution
program (or have the editor/plugin do it), markdown or flavor of markdown,
Agenda program (with plugin to read from the specific flavor of markdown) and
much more. While I would love for that to exist, nothing close to that exists
and the implementations that do dont interact well with each other at all(ie
agendas exist but dont support tables in markdown, code execution of any
language cant be used in spreadsheets, you cant have markdown syntax in a
spreadsheet, literate programming only supports markdown or their own version
etc...).


## Notes (markdown+tags+links+code) {#notes--markdown-plus-tags-plus-links-plus-code}

First thing that you should notice is that org mode provides a markdown like
syntax for its files. This syntax also pops out at you because it is color
coded and shows effects as you type it (ie <span class="underline">underline</span>). You can see a
example of this syntax on gitlab.com/renzix/Blog under blog.org. Lastly you
can fold/unfold bullets with tabs, check/uncheck boxes with standard emacs
keybindings, move entire bullets with holding alt and using the up/down
arrow etc... Also most keybinds are reused based on context(ie control c
control c if on a checkbox to check it, if on a heading add tags etc...).
Another nice thing thats not in markdown is that you can link to parts of
files, other parts of emacs(like erc the irc client) or external
programs(like shell or browsers).

Another thing is that you can make tags for each bullet. These tags are
easily searchable making finding specific notes easy to find. You can also
search multiple tags and see other information aswell in it making it much
better then markdown for notes. If your a computer science student or taking
notes related to programming it makes even more sense. You can put code
snippets and actually run them inside your notes.


## Documents (exporting) {#documents--exporting}

Like markdown org mode can be exported to be a document in many different
forms. Unlike markdown org mode gives you much more control over the output.
You can export to LaTeX, markdown, html, plain text, odt, pandoc (intern
getting docx), reveal.js and more. Not only that but you can actually
interact with the output in a standardized way. Say you want a specific css
when you export it as html and specific LaTeX code when doing it in LaTeX.
You can do that and have it export DIFFERENT PARTS with DIFFERENT styles
depending on the export. I had a mla style document at one point to write my
papers. You can even write you own exporter. I am writing this on ox-hugo a
seperate package from org mode which reads information from the files and
turns it into black friday markdown with the correct date/time/other stuff
after every save. Not only that but it only exports the specific bullet I am
in as each bullet is a seperate markdown file.


## Research/Code Documentation (code exec+exporting) {#research-code-documentation--code-exec-plus-exporting}

One of the most common things I see org mode used in is a literate config.
This allows you to write code in certain sections while leaving comments
making this easy to read. This is the very basic version of what literate
programming can do. While it is nice to just put :no export in order to
comment out a entire bullet related to a specific package or setting that
isn't what makes it powerful. The real power comes from stuff like code
documentation, college homework and Research papers where you are constantly
referencing code. When you can put this code INTO a readable format, press
C-c C-c to test it, have it ssh into remote servers, compose into eachother,
and use different languages with a single interface it makes doing code docs
MUCH easier.


## Finance Spreadsheets (Tables+code exec) {#finance-spreadsheets--tables-plus-code-exec}

I actually have a org file for my crypto which is really cool imo. It
basically pulls down information about each coin from the coingecko api in
python, my coinbase balence, and my gemini balence. I also put in some of my
other crypto manually because its on a cold wallet. I then put it into a
spreadsheet where I do math on it. Note that the math on it can either be in
M-x calc (which is a insanely powerful calculator in emacs capable of doing
derivitives) or elisp. These spreadsheets also pull information from
eachother so I have multiple that share the same info but I only have to
update one in order for the others to update. I havent experimented with
this but you could also use python(or whatever language that has libs) and
insert a image like a pie chart based on the information. Literally as
powerful as google sheets or excel.


## Scheduling (Agenda) {#scheduling--agenda}

This is prob my least known one beause I can never seem to use it and I dont
like any solutions on mobile for org mode. You can make a TODO list and
actively track what needs to get done, have multiple values of TODO (like
NEXT or WAITING), set dates/deadlines, track when you have completed them,
automatically move DONE items outside your todo into another file and more.
Not to mention its rly easy to set a TODO item (bind from anywhere within
emacs) and supports literally everything I said above. This stuff is
searchable through the agenda and saved in plain text wherever you tell it
to look. You can have multiple files or one file depending on what you want.
