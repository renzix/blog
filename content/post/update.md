+++
title = "Update"
author = ["Renzix"]
date = 2021-03-21T18:49:00-04:00
draft = false
+++

The website has been down for a VERY long time. So I have been too lazy to fix
it (and be able to post). This is mainly because I was self hosting everything
on a rpi through gitea and a nginx server (at one point apache). I don't feel
like throwing away 10$ a month so I am going to setup gitlab pages instead of a
real vps. Once I get a real place and start making ~100k or so a year I will
consider self hosting again.

To force myself to do this kind of stuff, I will probably put less thought and
talk about more subjects. I am currently obsessed with crypto/stocks/investing.
