+++
title = "Emacs Reborn"
author = ["Renzix"]
date = 2021-09-03T20:43:00-04:00
draft = false
+++

The other night i was thinking about how modern emacs rewrite would work and
what it would be. I was thinking why emacs is a single application and how to
justify that in the modern day. I dont think emacs goes far enough for
integration with the rest of the desktop and could go further. I came up with
the idea that emacs should not be a text editor/program but a compositor.

Okay, hear me out...

The real strengh of emacs is the integration between multiple apps inside a
single program. Having it be a compositor makes instant startup time (due to the
compositor just opening a window) and lets it have full integration with
external apps.

Now there is 2 ways to do something like this. The more vimy way would be to
create a bunch of sockets and have things ask said sockets for "special"
functions like window swallowing inside the app. The other(emacsy) way would to
have these apps built into the compositor. I think this is probably easier to
pull off but less modular.

So going into this idea the way emacs does it is by having a terminal based text
area for multiple applications. The windowing would obviously be through the
compositor but we have to edit text like emacs does. This is where the native
apps come in. These apps would support the compositor and either be part of the
compositors source code or able to talk to the socket which is read by the
compositor. If we have it builtin to the source code we could just draw it, if
it is socket based then we would have to use a different graphics api.

This will kinda be like a DE for power users with killer apps as opposed to easy
ones. The interoperablity will also be insane. One thing would be seemlessly
transitioning to native apps. 2 native apps would be a text editor and git
interface. The way I imagine it is like magit and normal emacs editing. Inside
the git interface you should be able to press enter on any of the files and see
the native editor like in magit. While that isnt different from normal emacs we
can do compositor specific stuff so global keybinds and controlling windows.

One of the nice things unique to a compositor would be to integrate a rofi/helm
like interface. I love how visual studio code does its control-p where you can
prefix it with > for a command etc. That would be great to have a default to
open programs and ! for commands or > to open files or something.

The other imo nessisary component is a file manager. I am unsure how I think
this should work but obviously it should have good integration with the git
interface and the text editor. This may include project functions like stuff in
projectile. I dont know whether it should be dual pane or if it should have
search on by default like a fuzzy finder. Maybe this entire part is unessisary
because of the existance of a robust search.
