+++
title = "The Cryptocurrency Argument"
author = ["Renzix"]
date = 2021-05-02T18:01:00-04:00
tags = ["crypto"]
draft = false
+++

I don't think people truly see the potential in crypto. To me crypto is
literally a breakthrough for financial technology. Right now is the very early
stages where machines are first getting invented.


## Corrupt government (adds competition) {#corrupt-government--adds-competition}

The fundemental reason why I think government currency is bad, is because it has
to be regulated. Luckily I live in the US where the fed is based, so I don't
really ever have to worry about currency as usd can't possibly fail and even if
it does, all world currencies do. If I did not live in the US this would be a
massive problem because 1 bad decision can lead to the destruction a entire
economy. Crypto never has this problem because you can't just "print more". Even
if you make the claim that developers can make the bad decision, just read the
paragraph below.

Another beautiful part about crypto is that you don't need loyalty to one crypto
or another (especially in the future). This solves the problem of bad leaders
which are leading your coin. Using cosmos sdk or exchanges (coming in the near
future like thorchain) you should be able to easily swap between cryptos. You
may think, but renzix that will cause a taxable event! While this is true 1. you
are only taxed on gains and 2. If you take out a loss that will offset a gain so
you should actively try to keep that at a net 0. Because of cryptos digital
nature it is very easy to track everything about the transaction and it should
get even easier on the future. I also hope that once crypto actually gets
adopted, that governments will change tax precedents to treat it more like a
currency and add a buffer on capital gains tax or something.

Even though governments may ban crypto, that tends to drive more people to
crypto.


## Moving Money {#moving-money}

The modern banking system is really broken because of all the checks it has to
do to function. It takes days and massive amounts of money to transfer funds
across countries when it takes even the worst cryptos minutes. There are plenty
of cryptos with extremely small fees which are safer and decentralized which you
can use already. Not to mention there is work being done so that the plumbing of
these financial institutions would be seemless so that you could transfer usd ->
eur in another country without even knowing that you used a crypto currency.
This use case alone will free up so much time/money.

The second use case is something like the lightning network, you could feasibly
make a true pay for exactly what you receive. A great example of this is a
netflix like service. Instead of a monthly reoccuring fee you could have every
second 0.0001 BTC gets pulled out of your account. This is interesting because
with stuff like XLM and the lightning network this is actually possible because
the fees are literally that low.

Relevant (probably not scams) crypto: bitcoin (with lightning network),
litecoin, bitcoin cash, ripple, stellar


## Programmable {#programmable}

Ethereum revolutionize crypto when it introduced smart contracts. The tldr of a
smart contract is just code that executes on the blockchain. Because it's on the
blockchain you can know exactly what code was ran so you do NOT have to rely on
a project saying it's open source but in reality it is not (and steals money
from you). The drawback is the gas fees however you are able to do whatever your
mind can think of (as long as the gas fee isn't too large) and not have to worry
about erasing or double spending money with clear rules on how the money will
flow. This really shines with specific use cases which is why ethereum has so
many different projects. Currently ethereum has by far the biggest ecosystem and
the most work done on it.

Relevant (probably not scams) projects: Ethereum, Cardano, Polkadot


### New coins {#new-coins}

Because you can program things, you can setup new coins which follow a specific
standard and use the same code base. The standard for ethereum is erc20. This
let's you piggyback off of etherums success as a project and be able to access
the ability to run smart contracts without making a entirely new vm. The price
that you pay is that you have to use ethereum to pay gas fees. Also there are
various other benefits like security and having a single wallet etc...

Also some coins offer specific gimmicks related to dapps or mining. Filecoin for
example let's you earn coins by serving files on the network. You can then use
these coins to access said files or just outright buy them. This kinda functions
as a scuffed AWS. Without the overhead of a real business with thousands of
employees.

Relevant (probably not scams) projects: Chainlink, Basic attention token, maker,
dai, usdc, compound, Filecoin, etc...


### Dapps {#dapps}

Dapps are the real reason why ethereum is so powerful. These are (normally) open
source applications which let's you do advanced things like banking,
loans(basically margin for crypto), automatic market making, and other services.
The 2 currently most popular is the collatorized loans (basically taking margin
out on your crypto) and dex's. Dexs are basically decentralized exchanges (like
coinbase) which let you trade crypto with random people. Normally this doesn't
work because the price would change rapidly but with the magic of automatic
market makers (amm) people will take both sides of the trade (with a small fee)
and eventually make money.

What a lot of dapps are doing is making their own token to force people to "buy
back in". They then give people these tokens and tell them they can vote on the
future of the protocol if they keep it. Not only that but these tokens tend to
appreciate due to people buying them to influence the protocol or people
thinking that this specific platform will be huge in the future.

This is only the very beginning of the use cases for dapps. With dapps you could
quite literally create companies without having a actual company and just pump
the related coin to the project. I could in theory start a taxi dapp which is
available for Android/ios. This app could be void of any corporate structure or
any notion of employees and just have investors of the taxicoin make decisions
related to it.

The problem which we haven't really faced yet (because dapps are so new) is that
the people with a massive amount invested might kill or ruin the project because
of greed or another reason. This is actually fixable if you build in safeguards
at the coin level which cannot be changed like not allowing 1 wallet to hold
more then 10% of the holdings or having a high amount of inflation given to the
drivers.

As shown above having the ability to have completely digital and decentralized
applications which give profits to it's users is insane to me. You could totally
define a COMPLETELY fair system of economics in which the people involved
getting all of the money.


### Nfts {#nfts}

Nfts are just like new coins, but instead of being the same, they are unique.
Erc720 defines what a general nft is. You can use nfts to prove you own a unique
code. The cool thing about nfts is that 1. You can't leak it without leaking
your own wallet address and 2. It's programmable just like any other coin inside
of the ethereum network.

The programmable part makes nfts insanely powerful. You can for example requires
a nft to login to a website or let you get backspace passes at a concert.
Another really interesting use case is for dapps by letting you use drm on
music/videos. Now you can force people to actually own the music/video and not
have to worry about the drm going out of service.

Artists can actually get paid for their work and you can own a song so long as
you have the nft for it(keys). Not only that but you could properly and
effortlessly transfer money to the multiple people in the song. This could all
happen instantaneously.

Lastly you could have a "minimum bid" for the song and the "highest bidders" get
access to backstage passes or access to a private discord group. All
automatically. Do you know how insane the music/movie industry is right now?
Billions of dollars are spent in ip rights because they are the only people who
know how to market/sell your song. With nfts there is a way for the average
person to do this without giving up their rights and STILL receive royalties (on
resale or just make a infinite supply).

Imo nfts don't have much useful things right now and is in a bubble. It needs
dapps, talented musicians and businesses to come in and abuse it as the ultimate
payment processor. Also keep in mind it is still the early adopter stage. Crypto
still needs to get more mainstream but the real life potential is still there.


## Privacy {#privacy}

There are multiple coins being created with the goal of being impossible to
track while still being secure. These coins may have different gimmicks but this
is very useful if you want to keep your privacy to only you. Most coins are
anonymous but not private so you would have to have 2 wallets of said coin and 1
of these coins to "clean" the coins to make that second wallet private. It can
be argued that complete privacy is a good or bad thing but privacy is even a
thing in the real financial world. These coins will take off, if governments
start banning crypto or other hiding/privacy is wanted/needed.

Another thing is that you can remove the idea of privacy for certain
people/accounts. If you enforce someone to only get paid in a nonprivate coin
(or even a company), you can see all their finances. This is kinda done in
public companies but its hard to prove for sure.

Related (probably not scams) projects: Monero, zcash, dash


## Governance {#governance}

I was debating on adding this because I am unsure if I covered this enough in
one of the programming sections. Governance is the ability for people who own
the coin to make changes to the protocol in a systematic way. It let's people
vote on proposals which can be added or removed by developers. One of the key
things to understand about governance is that unlike in stocks, this doesn't
have to mean ownership. The creator of a protocol can make rules and regulations
which coin holders can't overturn.

That being said governance has the potential to bleed the users of the protocol
out of money if used inside a dapp. Something like collatorized loans is
probably okay to take some of the profit and put it into the related governance
token but something like that uber app I mentioned above would not be unless
your giving the correspondent token directly to the dapp user and not burning
ones already on the market. Not to mention these tokens should NOT be
deflationary so that new users get a fair share and it doesn't become a "who
joined first"

Related cryptos: compound, aave, yfi, maker


## Crypto concerns {#crypto-concerns}

These are all of the concerns I can think of with crypto. I will say the only
lasting one is the environmental concerns and that both concerns could be said
about multiple different industries not just crypto. Not to mention that the
second part of that problem is 100% reversible and the first part is solvable
right now.


### Fees {#fees}

Currently fees on ethereum are insane. It honestly will take a very long time to
fix but we have the same technology in databases in a way. We just need to apply
to a secure blockchain.


### Environment {#environment}

I think crypto could in the future have a net 0 effect on the environment.
Currently governments attack businesses for being bad for the environment but
with crypto we have to make sure people are too. Fortunately for crypto, it is
still very profitable to use green energy to mine. We should be able to track
people polluting areas in the near future and be able to stop it.

The biggest problem comes in the processors and components that are thrown out
once something newer comes along (because it increases power efficiency
effectively making more use of electricity). This isn't just a crypto problem as
more and more smart phones and other devices are being made every year. There
needs to be some incentive to recycle these products.

Because at its core most cryptos are not private (its generally anonymous not
private), we can totally have exchanges with coins only mined with clean energy,
good working conditions and recycles their gpus. Kevin O'Leary for example does
this by investing in companies that create bitcoin while demanding he gets paid
in equity via bitcoin.


### Regulation {#regulation}

This probably will happen in the near future. Regulation has the ability to
stunt cryptos growth or let it go to the moon. Too little or too much regulation
will ultimately slow down crypto growth. I don't think too little will kill it
outright but it will cause a outright ban if not dealt with properly or cause a
stunt in growth. Too much regulation could cause it to take 100s of years and I
won't be able to see it grow in my lifetime.


### Marketing {#marketing}

Cryptos biggest problem is that it seems sketchy as fuck and it's not consumer
friendly. You can see that by looking at the price of bitcoin/dogecoin. They
both are ultimately trash yet they have huge market caps. This is because
everyone knows about bitcoin and a large amount of businesses added dogecoin to
their payment as a way to market their app to consumers. It let both the company
and dogecoin be on the front page of websites for a day.
