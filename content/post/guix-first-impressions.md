+++
title = "First Guixsd Impressions"
author = ["Renzix"]
date = 2019-11-18T21:43:00-05:00
tags = ["linux", "guix"]
draft = false
+++

I tried guixsd about a half a year ago and didn't like it. The reason being it
was super slow. A friend told me that guix has gotten faster and from my little
memory of it, it has by a decent amount. The laptop I am running it on is a 3gb
2 core thinkpad from 2005. Which was the same one I originally tried it on. It
was a little annoying for me because I originally had to build my kernel from
source as I don't have a free wifi card. The rest of this T400 works fine.

I have used nixos previously and liked it but found it annoying because I would
need to learn how the language works in order to do basic development stuff. Now
this is the same as guixsd however it is based on guile which is a scheme which
is a lisp which is very simple to learn imo.

Packaging software is actually awful but guixsd makes it alot easier. I packaged
ripgrep (which albeit was actually awful), hackrf and am in the process of
trying to package gnuradio and gqrx because I have to use it for my university
computer science seminar class.

I think I will eventually find guixsd as my main distro but I need to actually
learn guile and the packages guixsd provides. I have always liked the idea of
nixos and now that a lisp is offered with it I think I am on board. Maybe one
day I will package stuff like firefox for it and make it a really nice usable
distro.
