+++
title = "Common misconceptions about emacs keybindings"
author = ["Renzix"]
date = 2021-04-07T22:39:00-04:00
draft = false
+++

A lot of people dont understand where emacs keybindings come from. I am going to
give some reasons to what seems to be random keys and some ways to remember
them. First lets get some of the obvious things out of the way for most emacs users.


## Basics {#basics}

Emacs uses the term Meta for Alt. This is because the original keyboard had a
Meta key and not a Alt key. Most movement/editing keys are a single modifier and
a letter because that is very easy to press when compared to multiple modifiers
or one key after another. You can think of this like vims normal mode. If you
want to string commands together you can just hold the modifier. Lastly each
command has letters based on mnemonics of the function name not the location.
Stuff like next-line is C-n. Another thing is that keybindings do similar
commands with different sizes with more modifiers. C-f moves forward one line
and M-f moves forward one word, lastly C-M-f moves forward a paragraph. This
isnt just for movement as M-x and C-x are kinda similar and C-w is cut while M-w
is copy. Normally you want to have commands that you dont do one after another
which emacs does pretty well for most cases.


## Prefixes {#prefixes}

Another thing that emacs does is use a common prefix for groups of commands. The
most common one used is C-x which is a general prefix for everything that is
global (stuff like changing windows and saving buffers). C-x also has like
things either mnemonic or next to each other. C-x 1 through C-x 4 are all about
window movement for example. C-c is for user and major/minor mode commands.


## Other applications {#other-applications}

Emacs keybinds are actually used outside of emacs too. These binds dont have
prefixes and are mostly JUST for movement. The two areas I know of are command
line and macOS text buffers. Thats right most of macOS supports emacs keybinds
while most of linux doesnt. Even vscode and sublime text support these by
default inside macOS. This is mainly because macOS tells the application to use
the command key (windows key) and alt for general purpose stuff and reserves
control for the system. This also includes applications like safari (which is
why people get confused when C-a doesnt work to highlight all like in windows).
Command line is another use and is MUCH better then modal keybinds simply
because you arent editing command line commands, only typing them. There are
even tricks like up arrow and !! to avoid having to edit this.
