+++
title = "Monster 2"
author = ["Renzix"]
date = 2024-01-31T22:22:00-05:00
tags = ["story"]
draft = false
+++

As the air flows to the east through the cage door a whistling sound can be heard. Inside this cage is a monster but unlike normal monsters who roam the earth this monster stays here.

While locked up his cage the monster hears something. The sound seems all too familar. It is as if someone was weeping. Concerned but deterrmined to not leave his prison the monster yells out "Hello? Is anyone there?". The weeping suddenly stopped and the sound of leaves crunching start to occur with each second getting quieter. It seemed that the creature scuttled away.

The next week a pack of college students lead by a lone college professor came through. The monster accustom to this encounter twice a year and tried to stay quiet to avoid scaring them. Unfortunately that did not work this year as a curious girl noticed the solid metal door. "Hey prof! Whats this?", scared that people will find him the monster loudly whispered "shhh". Quick on her feet the girl picked up a stick with some dark green mould from the ground and hurried back to the group. "Ahh yes, this is a common fungus that grows in the forest its called..." the college professor began, examining the stick that the girl brought back. The monster still hidden sighed a breath of relief grateful for the girls quick thinking.

The class of students quickly left and the rest of the day felt normal to the monster. That was until in the evening when the monster heard a familar voice. "HEY!", it seemed the girl from earlier came back... "Quiet down, I can hear you fine" whispered the monster. Undeterred, the girl grinned, "What are you doing in there?". The monster responded, "This is my home, I cannot leave it". "Why?" the girl asked. "I do not know" said the monster. "If you don't know then why not try and see what happens?" urged the girl. "NO" the monster said loudly. The sudden outburst startled birds perched in nearby trees, causing them to take flight.

The monster, realizing the impact of his loud refusal on the peaceful surroundings, took a deep breath and softened his tone. "I'm sorry," he rumbled apologetically. "Its okay said the girl, I was pushing too hard. What is your name?". "I do not have a name, nor do I need one" murmured the monster. "Well, I think everyone deserves a name. How about Mound, for the mound you're stuck in?" The monster considered this for a moment before finally nodding in agreement. "Mound it is" proclaimed the monster. Mound and the girl talked until the sun went down about the wonders of the forest and of the world beyond the cage. As the sunset started to paint the sky Mound realized that all good things must come to a end. The girl stood up and said "Well I have to head back now". "Before you leave, what is your name?" mound asked. "Sophie, I can come next week if you would like" sophie said. I would love that Mound said with a grin.

Thus started weekly get togethers with Mound and Sophie. She would come and sprout vivid detail about different types of monsters and different types of places. One day she would talk of a Bylanth, a large flying black snake like creature which when threatened or excited puffs out part of the scales near its head. Another day she would talk about the mystical Lava Dome biome where there are lava geyser sprouting molten rock which dries in the air forming delicate domes which collapse back into itself during heavy rain or due to the presence of birds.

Mound however knew the inevidible truth that Sophie was not from around here. Her time at college was coming to a end. Worried about this Mound tried brining it up. "Sophie, what will happen when you graduate" he inqired with a voice of partial concern for what comes next. "I will come back and visit every once in a while, you aren't getting rid of me that easily" she asserted. Eventually she graduated and went home. Mound patiently waited for her to return but that day never came. Mound eventually realized and started to weep.
