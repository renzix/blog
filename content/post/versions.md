+++
title = "More versions of this blog!!!"
author = ["Renzix"]
date = 2019-11-13T23:46:00-05:00
tags = ["emacs", "blog", "git"]
draft = false
+++

Someone asked for for a plain text version trying to meme me so I am making
plain text and html exports available. I can do this because the blog is all a
single org mode file which I can just export with this bash command provided
things are installed and because git hooks are amazing. You also need
a package called htmlize for emacs on melpa which was annoying to get working.
PS: They are available at both  <http://renzix.com/blog.txt> and
<http://renzix.com/blog.html> respectively.

```sh
emacs --batch --eval "(require 'org)" --eval "(package-initialize)" blog.org -f org-ascii-export-to-ascii -f org-html-export-to-html
```
