+++
title = "About"
author = ["Renzix"]
draft = false
+++

## Welcome to my Website {#welcome-to-my-website}

I spend alot of time messing around in Linux and Emacs. This used to be self
hosted on my rpi but I moved it to gitlab so I dont have to worry about self
hosting. This blog is going to be things I am passonate about which is probably
opensource software, investing, maybe marketing and philosophy?


## Work Experience {#work-experience}


### Internship Picitiny Arsenal {#internship-picitiny-arsenal}

At Picitiny Arsenal I was apart of the CROWS Team at the Department of Defense
which worked on developing and acquiring the [CROWS](https://duckduckgo.com/?q=crows+military&t=ht&ia=web). It was during the summer and
I spent most of my time with the acquisition team reviewing documents containing
low level design ideas and creating a frontend for a C# database in wpf.


### Parsons {#parsons}

I am currently a associate software engineer at parsons. I am contracted to work
at piccatinny arsenal and am currently working on cpp code. I got a CAC and will
be on DREN soon.
