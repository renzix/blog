+++
title = "Resume"
author = ["Renzix"]
draft = false
+++

## Welcome to my Website {#welcome-to-my-website}

I am Daniel DeBruno a student who is studying Computer Science at WPUNJ. I spend
alot of time messing around in Linux and Emacs. This server is self hosted at my
machine at home running Fedora and a multitude of sevices. I'm going to treat
this about page as a little resume thingy for now. PS I didn't use https for
this part of the website(nextcloud and gitea does have it) because `I DON'T NEED
UNNECESSARY ABSTRACTION.`


## Technologies {#technologies}


### Programming</h2> {#programming-h2}

The first college I went to taught Java so that is the one I know the most
about. Generally I find programming paradigms very interesting so I am always
trying to understand how different languages work. The other languages I would
consider myself proficient for a college kid is C, C++ and python. I know alot
of other general knowledge ie a decent amount of posix/bash shell and a bunch of
other random languages.


### Version Control {#version-control}

My only experience with version control is through git. I tend to start alot of
projects and abandon them so through the process I ended up learning alot about
git. The man pages for git are also REALLY nice and there are a couple good
websites [One of which I used alot](https://www.atlassian.com/git). I also made a little [presentation for my
computer science club explaining git](https://github.com/Renzix/Lectures") via emacs org mode. I know bare basics of
some other vcs's like mecurial and svn.


### Linux {#linux}

After using Windows for my entire life my Dad got me a macbook to learn so I
could help him with macOS. This got me very curious about operating systems and
a couple years later I looked into Linux. This was about 2014 or 2015 and as of
2017 I switched everything to Linux and haven't looked back. As I said before
this server runs on Fedora, my main PC is on Gentoo and I switch around with my
laptop alot(currently testing fedora silverblue). I learned a tremendous amount
about how everything works with Linux and I can confidently say it has been one
of my greatest pations in life.


## Work Experience {#work-experience}


### Internship Picitiny Arsenal</h2> {#internship-picitiny-arsenal-h2}

At Picitiny Arsenal I was apart of the CROWS Team at the Department of Defense
which worked on developing and acquiring the [CROWS](https://duckduckgo.com/?q=crows+military&t=ht&ia=web). It was during the summer and
I spent most of my time with the acquisition team reviewing documents containing
low level design ideas and creating a frontend for a C# database in wpf.
